console.log("hello world");

// khai bao khoi tao gia tri cho bien
var username = "Alice";

var address = "hcm";

//gan  lai gia tri cho bien re-assign
address = "hn";

//camel case
var diemHocVien;

//snack case
var diem_hoc_vien;

//console.log("Alice")

//data type
// string
var job = "Grab";

//number
var phoneNumber = 26259598;

//boolean
var islogin = true;
//var islogin = false;

var tenNguoiYeu = null;

tenNguoiYeu = "Alice";
//=> pass by value

var num1 = 100 + 10000;
console.log("num1", num1)

var num2 = 20 * 2;
console.log("num2", num2)

var num3 = 100/2;
console.log("num3", num3)

var num4 = 10 % 4;
console.log("num4", num4)

var num7 = 2;
//num7 = ++num7 + 10;
// num7 = num7 + 10;
//num7 += 10;
num7 = num7++ + 10;
console.log("num7", num7)

var num10;

const HOLIDAYS_COUNT = 12;


